package com.cards.controller;

import com.cards.entity.Cards;
import com.cards.entity.HttpResponse;
import com.cards.exception.CustomException;
import com.cards.repository.CardsRepository;
import com.cards.service.UsersService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

@RestController
@RequestMapping("/cards/v1")
public class CardsController {
    @Autowired
    CardsRepository cardsRepository;
    @Autowired
    private UsersService userService;

    private static final ExampleMatcher SEARCH_CONDITIONS_MATCH_ANY = ExampleMatcher
            .matching()
            .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.exact())
            .withMatcher("color", ExampleMatcher.GenericPropertyMatchers.exact().ignoreCase())
            .withMatcher("status", ExampleMatcher.GenericPropertyMatchers.exact().ignoreCase())
            .withMatcher("datecreated", ExampleMatcher.GenericPropertyMatchers.exact().ignoreCase())
            .withIgnoreNullValues();

    /*
     * create record
     */
    @PostMapping("/create")
    public ResponseEntity<Cards> create(@RequestBody Cards cards) throws CustomException {

        return new ResponseEntity<>(cardsRepository.save(cards), HttpStatus.CREATED);
    }

    /*
     * returns all the records
     */
    @GetMapping("/view")
    public ResponseEntity<List<Cards>> view() {

        List<Cards> projects = new ArrayList<Cards>();

        cardsRepository.findAll().forEach(projects::add);

        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @GetMapping("/view/{id}")
    public ResponseEntity<Cards> getCardById(@PathVariable("id") long id) throws CustomException {
        Cards cards = cardsRepository.findById(id)
                .orElseThrow(() -> new CustomException("Record not found id: " + id));

        return new ResponseEntity<>(cards, HttpStatus.OK);
    }

    @PostMapping("/search")
    public ResponseEntity<List<Cards>> searchCards(@RequestBody String request) {

        JSONObject jobj = new JSONObject(request);

        Cards cards = Cards.builder()
                .name(jobj.has("name")?jobj.getString("name"):null)
                .color(jobj.has("color")?jobj.getString("color"):null)
                .status(jobj.has("status")?jobj.getString("status"):null)
                .datecreated(jobj.has("datecreated")? (Date) jobj.get("datecreated") :null)
                .build();

        Example<Cards> example = Example.of(cards, SEARCH_CONDITIONS_MATCH_ANY);

        List<Cards> cards1 = cardsRepository.findAll(example);

        return new ResponseEntity<>(cards1, HttpStatus.OK);
    }

    /*
     * update record
     */
    @PutMapping("/update/{id}")
    public ResponseEntity<Cards> update(@PathVariable("id") long id, @RequestBody Cards cards) throws CustomException {

        Cards cards1 = cardsRepository.findById(id)
                .orElseThrow(() -> new CustomException("Record Not found"));

        cards1.setName(cards.getName());
        cards1.setDescription(cards.getDescription());
        cards1.setColor(cards.getColor());

        return new ResponseEntity<>(cardsRepository.save(cards1), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<HttpResponse> deleteUser(@PathVariable("id") long id) {
        userService.deleteUser(id);
        return response(HttpStatus.OK, "User deleted successfully.");
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus,
                httpStatus.getReasonPhrase().toUpperCase(), message.toUpperCase()), httpStatus);
    }

}
