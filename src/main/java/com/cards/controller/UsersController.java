package com.cards.controller;

import com.cards.entity.HttpResponse;
import com.cards.entity.Users;
import com.cards.filter.JWTTokenProvider;
import com.cards.entity.UserPrincipal;
import com.cards.repository.UsersRepository;
import com.cards.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/cards/v1")
public class UsersController {
    private AuthenticationManager authenticationManager;
    private UsersService usersService;
    private JWTTokenProvider jwtTokenProvider;
    private UsersRepository usersRepository;

    @Autowired
    public UsersController(AuthenticationManager authenticationManager, UsersService usersService, JWTTokenProvider jwtTokenProvider, UsersRepository usersRepository) {
        this.authenticationManager = authenticationManager;
        this.usersService = usersService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.usersRepository = usersRepository;
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody Users user) {
        authenticate(user.getEmail(), user.getPassword());
        Users loginUser = usersService.findUserByEmail(user.getEmail());
        UserPrincipal userPrincipal = new UserPrincipal(loginUser);
        Map<String, Object> respMap = new HashMap<>();
        respMap.put("token", jwtTokenProvider.generateJwtToken(userPrincipal));
        respMap.put("expires_in",60);

        return new ResponseEntity<>(respMap, HttpStatus.OK);
    }

    private void authenticate(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus,
                httpStatus.getReasonPhrase(), message), httpStatus);
    }

}
