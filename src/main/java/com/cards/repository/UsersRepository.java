package com.cards.repository;

import com.cards.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users,Long> {
    Users findUserByEmail(String email);
}
