package com.cards.entity;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Builder
@Table(name = "card")
public class Cards {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    @Column(nullable = false)
    private String name;
    private String description;
    @Pattern(regexp = "^#([A-Fa-f0-9]{6})$", message = "Color should be 6 alphanumeric characters prefixed with #")
    private String color;
    private String status = "To Do";
    @CreationTimestamp
    private Date datecreated;
    @UpdateTimestamp
    private Date dateProcessed;
}
