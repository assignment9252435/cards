package com.cards.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;
    private String email;
    private String userType;
    private String password;
    private boolean isActive=true;
    private String[] authorities;
    @CreationTimestamp
    private Date datecreated;
    @UpdateTimestamp
    private Date dateProcessed;

    public Users(String email, String userType, String password) {
        this.email = email;
        this.userType = userType;
        this.password = password;
    }

}
