package com.cards.service;

import com.cards.entity.Users;

import java.util.List;

public interface UsersService {

    List<Users> getUsers();
    Users findUserByEmail(String email);
    void deleteUser(long id);
}


